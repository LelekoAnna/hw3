let isGame = confirm('Do you want to play a game?');
const findElement = id => document.getElementById(`${id}`);

let max;
let attempts;
let posPrize;
let prizePerRound;
let prize = 0
let number;

const container = findElement("main");
const maxNumber = findElement("maxNumber");
const attemptsLeft = findElement("attemptsLeft");
const totalPrize = findElement("totalPrize");
const possiblePrize = findElement("possiblePrize");
const usersNumber = findElement("usersNumber");

const startGame = (maxNumber, win) => {
    number = Math.floor(Math.random() * maxNumber);
    attempts = 3;
    prizePerRound = win;
    posPrize = win;
    max = maxNumber;
    updateInfo();
}

const updateInfo = () => {
    maxNumber.innerText = max;
    attemptsLeft.innerText  = attempts;
    totalPrize.innerText  = prize;
    possiblePrize.innerText = prizePerRound;
}

const continueGame = () => {
    if(isGame) {
        startGame(max * 2, posPrize * 3);
    } else {
        console.log(`Thank you for a game. Your prize is: ${prize}$`);
    }
}

const checkNumber = () => {
    if(usersNumber.value == number){
        prize = prize + prizePerRound;
        isGame = confirm(`Your prize is: ${prize}$. Do you want to continue a game?`);
        continueGame();
    } else {
        prizePerRound = Math.floor(prizePerRound/2);
        attempts -= 1;
        if(attempts < 1){
            console.log(`Thank you for a game. Your prize is: ${prize}$`);
            isGame = confirm('Do you want to play a game?');
            playNewGame();
        }
    }
    updateInfo();
    usersNumber.value = '';
};

const playNewGame = () => {
   if(isGame){
       container.style.display="block";
       startGame(5, 10);
    } else {
        console.log('You did not become a millionaire')
    } 
}

playNewGame();