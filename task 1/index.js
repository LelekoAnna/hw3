const fighter = (object) => {
    const name = object.name;
    const attack = object.attack;
    let hp = object.hp;
    let wins = 0;
    let loses = 0;
    const setWins = () => {
        wins +=1;
    };
    return {
        getFighterName: () => {
            return name;
        },
        block: () => {
            return false;
        },
        getFighterStats: () => {
            return object;
        },
        showResult: () => {
            return (`
                Fighter ${name}
                - Combat stats: { wins: ${wins}, loses: ${loses} };
                - Properties: { name: ${name}, attack: ${attack}, hp: ${hp}};
            `);
        },
        fight: (enemy) => {
            let enemyStats = enemy.getFighterStats();
            if(enemyStats.hp <= attack) {
                setWins();
                enemy.setLoses();
            }
            enemy.setHp(attack);
        },
        setLoses: () => {
            loses +=1;
        },
        setHp: (damage) => {
            hp = hp-damage;
            if(hp < 0) {
                hp = 0
            }
        }
    }

}

const fighter1 = fighter({name: 'John', attack: 100, hp: 100});
const fighter2 = fighter({name: 'Kayn', attack: 50, hp: 300});
const fighter3 = fighter({name: 'Bill', attack: 40, hp: 100});

fighter1.fight(fighter2);
fighter1.fight(fighter3);

console.log(fighter1.showResult());
console.log(fighter2.showResult());
console.log(fighter3.showResult());